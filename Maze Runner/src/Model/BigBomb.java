package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class BigBomb extends Bomb {

	// the amount of damage will be discussed.
	private final int bigBombDamage;
	// icon path will be added by designer.
	
	private static Image bigBombIcon = new ImageIcon("./icon/fixed/bomb/bomb1.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);

	public BigBomb(int difficulty) {
		if(difficulty == 1){
			bigBombDamage = 30;
		} else if (difficulty == 2){
			bigBombDamage = 40;
		} else {
			bigBombDamage = 50;
		}
		setDamage(bigBombDamage);
		setEffect();
		setIcon(bigBombIcon);
	}
	
	
	public void setIcon(Image newIcon) {
		bigBombIcon = newIcon;
	}
	
	public Image getIcon() {
		return bigBombIcon;
	}
}
