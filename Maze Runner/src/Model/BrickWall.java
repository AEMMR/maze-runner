package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class BrickWall extends Wall{
	
	private static Image icon;
	
	public BrickWall(){
		BrickWall.icon = new ImageIcon("./icon/wall/wall0.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);
		setDestructable(false);
		setIcon(icon);
	}

	public void setIcon(Image newIcon) {
		BrickWall.icon = newIcon;
	}
	
	public Image getIcon() {
		return BrickWall.icon;
	}
	
}
