package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Bullet extends Item {
	private Collides collides = new BulletCollide();
	
	private Image icon = new ImageIcon("./icon/animation/fire/fire0.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);
	public boolean collision(Item item) {
		return collides.collide(this, item);
	}


	@Override
	public void setIcon(Image newIcon) {
		icon = newIcon;
	}


	@Override
	public Image getIcon() {
		return icon;
	}
}
