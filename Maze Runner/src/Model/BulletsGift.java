package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class BulletsGift extends Gift{
	
	int addedBullets;
	
	private Image icon = new ImageIcon("./icon/fixed/gift/ammo.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);
	
	public BulletsGift(int difficulty){
		if(difficulty == 1){
			addedBullets = 6;
		} else if (difficulty == 2){
			addedBullets = 4;
		} else {
			addedBullets = 2;
		}
		setEffect();
	}

	@Override
	public void setEffect() {
		effect = addedBullets;
		
	}

	@Override
	public void setIcon(Image newIcon) {
		icon = newIcon;
	}

	@Override
	public Image getIcon() {
		return icon;
	}

}
