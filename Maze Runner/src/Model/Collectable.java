package Model;

public abstract class Collectable extends Item{
	
	protected int effect;

	public int getEffect() {
		return effect;
	}

	public abstract void setEffect();
	
	
}
