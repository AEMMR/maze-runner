package Model;

import sound.SoundContext;
import sound.StartSound;

public interface Collides {
	public boolean collide(Item collider, Item item);
}

class PlayerCollide implements Collides {

	public boolean collide(Item collider, Item item) {
		Player player = (Player) collider;
		if (item.getClass().getSuperclass() == Wall.class) {
			return false;
		} else {
			// gifts effects here.
			if (item.getClass().getSuperclass() == Gift.class) {
				// health gifts
				if (item.getClass() == HealthGift.class) {
					SoundContext context = new SoundContext("./musics/gift.wav");
					StartSound start = new StartSound();
					context.setState(start);	
					context.doAction();
					player.addScore(250);
					int health = player.getHealth();
					health += ((Collectable) item).getEffect();
					if (health > 100) {
						health = 100;
					}
					player.setHealth(health);
					// bullets Gift
				} else if (item.getClass() == BulletsGift.class) {
					SoundContext context = new SoundContext("./musics/health.wav");
					StartSound start = new StartSound();
					context.setState(start);	
					context.doAction();
					player.addScore(250);
					Gun gun = player.getGun();
					gun.addBullets(((Collectable) item).getEffect());
					player.setGun(gun);
				}
			}
			// bombs effects here.
			if (item.getClass().getSuperclass() == Bomb.class) {
				player.subtractScore(150);
				int health = player.getHealth();
				int lives = player.getLives();
				health += ((Collectable) item).getEffect();
				if (health <= 0) {
					if (lives > 0) {
						SoundContext context = new SoundContext("./musics/lostLife.wav");
						StartSound start = new StartSound();
						context.setState(start);	
						context.doAction();
						lives--;
					} else {
						SoundContext context = new SoundContext("./musics/death.wav");
						StartSound start = new StartSound();
						context.setState(start);	
						context.doAction();
						System.exit(0);
					}
				}
				player.setHealth(health);
				player.setLives(lives);
			}
			// getting killed here.
			if (item.getClass().getSuperclass() == Monster.class) {
				player.subtractScore(200);
				int lives = player.getLives();
				if (lives > 0) {
					SoundContext context = new SoundContext("./musics/lostLife.wav");
					StartSound start = new StartSound();
					context.setState(start);	
					context.doAction();
					lives--;
				} else {
					SoundContext context = new SoundContext("./musics/death.wav");
					StartSound start = new StartSound();
					context.setState(start);	
					context.doAction();
					System.out.println("3al donia el salankateeh");
					System.exit(0);
				}
				player.setLives(lives);
				return false;
			}
			return true;
		}

	}

}

class BulletCollide implements Collides {

	public boolean collide(Item collider, Item item) {
		Player player = Player.getInstance();
		SoundContext context = new SoundContext("./musics/bullet.wav");
		StartSound start = new StartSound();
		context.setState(start);	
		context.doAction();
		if (item.getClass().getSuperclass() == Wall.class) {
			if (((Wall) item).isDestructable()) {
				player.addScore(100);
				return true;
			} else {
				return false;
			}

		} else if (item.getClass().getSuperclass() == Gift.class) {
			player.subtractScore(50);
			// destroy gift or do nothing.
			return true;// do nothing

		} else if (item.getClass().getSuperclass() == Bomb.class) {
			player.addScore(100);
			// destroy bomb.
			return true;
		} else if (item.getClass().getSuperclass() == Monster.class) {
			player.addScore(300);
			// Monster is Dead or damaged.
			return true;
		} else {
			return false;
		}
	}
}
