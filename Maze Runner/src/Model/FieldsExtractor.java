package Model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;

public class FieldsExtractor {

	private LinkedList<Object> methods;
	private LinkedList<String> names;

	public FieldsExtractor() {
		methods = new LinkedList<Object>();
		names = new LinkedList<String>();
	}

	public void extract(Object o) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method[] temp = o.getClass().getMethods();
		for (Method m : temp) {
			if (m.getName().indexOf("get") == 0) {
				names.add(m.getName());
				methods.add(m.invoke(o));
			}
		}
	}

	public boolean hasNext() {
		return methods.size() > 0;
	}

	public String nextName() {
		return names.poll();
	}

	public Object nextValue() {
		return methods.poll();
	}
}
