package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class HealthGift extends Gift{

	int addedHealth ;
	
	private Image icon = new ImageIcon("./icon/fixed/gift/health.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);
	
	public HealthGift(int difficulty){
		if(difficulty == 1){
			addedHealth = 80;
		} else if (difficulty == 2){
			addedHealth = 50;
		} else {
			addedHealth = 30;
		}
		setEffect();
	}

	@Override
	public void setEffect() {
		effect = addedHealth;
		
	}

	@Override
	public void setIcon(Image newIcon) {
		icon = newIcon;
	}

	@Override
	public Image getIcon() {
		return icon;
	}
}
