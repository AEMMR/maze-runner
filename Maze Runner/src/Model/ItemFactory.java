package Model;

public class ItemFactory {

	int difficulty;

	public ItemFactory(int difficulty) {
		this.difficulty = difficulty;
	}

	/**
	 * 
	 * @param newItemType :
	 *            brickWall, woodWall, healthGift, bulletsGift, bigBomb,
	 *            smallBomb, bigSlowMonster, smallFastMonster, endCell, emptyCell or player.
	 * @return
	 */
	public Item newInstance(String newItemType) {

		if (newItemType.equals("brickWall")) {

			return new BrickWall();

		} else if (newItemType.equals("woodWall")) {

			return new WoodWall();

		} else if (newItemType.equals("healthGift")) {

			return new HealthGift(difficulty);

		} else if (newItemType.equals("bulletsGift")) {

			return new BulletsGift(difficulty);

		} else if (newItemType.equals("bigBomb")) {

			return new BigBomb(difficulty);

		} else if (newItemType.equals("smallBomb")) {

			return new SmallBomb(difficulty);

		} else if (newItemType.equals("bigSlowMonster")) {

			return new BigSlowMonster(difficulty);

		} else if (newItemType.equals("smallFastMonster")) {

			return new SmallFastMonster(difficulty);

		} else if (newItemType.equals("emptyCell")) {

			return new EmptyCell();

		}else if (newItemType.equals("endCell")) {

			return new EndCell();

		}else if (newItemType.equals("player")) {

			return Player.getInstance();

		}
		return null;

	}

}
