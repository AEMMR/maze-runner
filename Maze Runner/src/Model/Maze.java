package Model;

import java.util.ArrayList;
import java.util.Random;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import control.MyLogger;
import view.MazeObserver;

public class Maze {

	private Item[][] mazeArray;
	private Point[] monsters;
	private Integer difficulty;
	public Player runner;
	private ItemFactory itemFactory;
	private ArrayList<Item> hiddenItems;
	private int updates, lives;
	private int numberOfRows, numberOfColumns;
	private Point endPoint;
	long startTime, endTime;
	private MazeObserver myObserver;

	private static final Logger LOGGER = MyLogger.getInstance();
	
	public Maze(Integer Difficulty) {
		LOGGER.info("Initializng the maze...");
		itemFactory = new ItemFactory(Difficulty);
		runner = (Player) itemFactory.newInstance("player");
		runner.init();
		lives = runner.getLives();
		numberOfRows = 35;
		numberOfColumns = 63;
		mazeArray = new Item[numberOfRows][numberOfColumns];
		hiddenItems = new ArrayList<Item>();
		monsters = new Point[Difficulty];
		this.difficulty = Difficulty;
		updates = 0;
		startTime = System.currentTimeMillis();
		GenerateMaze();
	}

	public void setMazeArray(Item[][] mazeArray) {
		LOGGER.info("Setting maze array...");
		this.mazeArray = mazeArray;
		int k = 0;
		for (Item[] i : mazeArray) {
			for (Item j : i) {
				if (j.getClass().getSuperclass() == Monster.class) {
					monsters[k++] = j.getPoistion();
				}
			}
		}
	}
	
	public int getDifficulty() {
		return difficulty;
	}

	private void addItems() {
		LOGGER.info("Adding items to the maze...");
		// MONSTERS
		for (int i = 0; i < monsters.length; i++) {
			Random randomizer = new Random();
			int x = 0;
			int y = 0;
			while (mazeArray[x][y].getClass().getSuperclass() == Wall.class
					|| mazeArray[x][y].getClass() != EmptyCell.class) {
				x = randomizer.nextInt(numberOfRows - 1);
				y = randomizer.nextInt(numberOfColumns - 1);
			}
			monsters[i] = new Point(x, y);
			if (i % 2 == 0) {
				Item monster = itemFactory.newInstance("bigSlowMonster");
				monster.setPosition(new Point(x, y));
				((Monster) monster).initTarget();
				mazeArray[x][y] = monster;
			} else {
				Item monster = itemFactory.newInstance("smallFastMonster");
				monster.setPosition(new Point(x, y));
				((Monster) monster).initTarget();
				mazeArray[x][y] = monster;
			}

		}
		LOGGER.info("Monsters added.");
		// GIFTS
		for (int i = 0; i < 6 - difficulty; i++) {
			Random randomizer = new Random();
			int x = 0;
			int y = 0;
			while (mazeArray[x][y] == null || mazeArray[x][y].getClass() != EmptyCell.class) {
				x = randomizer.nextInt(numberOfRows - 1);
				y = randomizer.nextInt(numberOfColumns - 1);
			}
			if (i % 2 == 0) {
				Item healthGift = itemFactory.newInstance("healthGift");
				healthGift.setPosition(new Point(x, y));
				mazeArray[x][y] = healthGift;
			} else {
				Item bulletsGift = itemFactory.newInstance("bulletsGift");
				bulletsGift.setPosition(new Point(x, y));
				mazeArray[x][y] = bulletsGift;
			}
		}
		LOGGER.info("Gifts added.");
		// BOMBS
		for (int i = 0; i < 6 - difficulty; i++) {
			Random randomizer = new Random();
			int x = 0;
			int y = 0;
			while (mazeArray[x][y] == null || mazeArray[x][y].getClass() != EmptyCell.class) {
				x = randomizer.nextInt(numberOfRows - 1);
				y = randomizer.nextInt(numberOfColumns - 1);
			}
			if (i % 2 == 0) {
				Item bigBomb = itemFactory.newInstance("bigBomb");
				bigBomb.setPosition(new Point(x, y));
				mazeArray[x][y] = bigBomb;
			} else {
				Item smallBomb = itemFactory.newInstance("smallBomb");
				smallBomb.setPosition(new Point(x, y));
				mazeArray[x][y] = smallBomb;
			}
		}

		// wooden walls
		for (int i = 0; i < 10 - difficulty; i++) {
			Random randomizer = new Random();
			int x = 0;
			int y = 0;
			while (mazeArray[x][y] == null || mazeArray[x][y].getClass() != EmptyCell.class) {
				x = randomizer.nextInt(numberOfRows - 1);
				y = randomizer.nextInt(numberOfColumns - 1);
			}

			Item woodWall = itemFactory.newInstance("woodWall");
			woodWall.setPosition(new Point(x, y));
			mazeArray[x][y] = woodWall;

		}
		LOGGER.info("Wooden walls added.");
		// End
		int x = 0;
		Random randomizer = new Random();
		while (x == 0 || mazeArray[x][mazeArray[0].length - 2].getClass().getSuperclass() == Wall.class) {
			x = randomizer.nextInt(numberOfRows - 1);
		}
		mazeArray[x][mazeArray[0].length - 1] = new EndCell();
		endPoint = new Point(x, mazeArray[0].length - 1);
	}

	public boolean win() {
		if (runner.getPoistion().x == endPoint.x && runner.getPoistion().y == endPoint.y) {
			LOGGER.info("Player wins.");
			return true;
		} else {
			return false;
		}
	}
	
	public boolean lose() {
		if (runner.getLives() == 0) {
			LOGGER.info("Player loses.");
			return true;
		} else {
			return false;
		}
	}

	// the extra walls on the bottom and right edges are because of
	// the even values of numberOfRows and numberOfColumns
	private ArrayList<Pair<Point, Point>> getEdges() {
		ArrayList<Pair<Point, Point>> edges = new ArrayList<Pair<Point, Point>>();
		for (int i = 1; i < numberOfRows - 1; i += 2) {
			for (int j = 1; j < numberOfColumns - 1; j += 2) {
				int endx = i + 2;
				int endy = j + 2;
				if (endx < numberOfRows - 1) {
					edges.add(new Pair<Point, Point>(new Point(i, j), new Point(endx, j)));
				}
				if (endy < numberOfColumns - 1) {
					edges.add(new Pair<Point, Point>(new Point(i, j), new Point(i, endy)));
				}
			}
		}
		return edges;
	}

	private void GenerateMaze() {
		LOGGER.info("Generating the maze...");
		// the generated maze is a perfect maze (a spanning tree)
		ArrayList<Pair<Point, Point>> edges = new ArrayList<Pair<Point, Point>>();
		edges = getEdges();
		ArrayList<ArrayList<String>> sets = new ArrayList<ArrayList<String>>();
		int size = edges.size();
		for (int i = 1; i < this.numberOfRows - 1; i += 2) {
			for (int j = 1; j < this.numberOfColumns - 1; j += 2) {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(new Point(i, j).toString());
				sets.add(temp);
			}
		}
		for (int i = 0; i < size; i++) {
			Random randomizer = new Random();
			int pick = randomizer.nextInt(edges.size());
			if (i == 0) {
				pick = 0;
			}
			Point first = edges.get(pick).getL();
			Point second = edges.get(pick).getR();
			String fString = first.toString();
			String sString = second.toString();
			edges.remove(pick);
			int fIndex = 12345, sIndex = 1234;
			for (int j = 0; j < sets.size(); j++) {
				if (sets.get(j).contains(fString)) {
					fIndex = j;
				}
				if (sets.get(j).contains(sString)) {
					sIndex = j;
				}
			}
			if (i == 0) {
				int x = first.x;
				int y = first.y;
				runner.setPosition(new Point(x, y));
				mazeArray[x][y] = runner;
			}
			if (fIndex != sIndex) {
				int x1 = first.x;
				int y1 = first.y;
				int x2 = second.x;
				int y2 = second.y;
				int x3 = (x1 + x2) / 2;
				int y3 = (y1 + y2) / 2;

				if (mazeArray[x1][y1] != runner) {
					mazeArray[x1][y1] = new EmptyCell();
				}
				if (mazeArray[x2][y2] != runner) {
					mazeArray[x2][y2] = new EmptyCell();
				}
				if (mazeArray[x3][y3] != runner) {
					mazeArray[x3][y3] = new EmptyCell();
				}
				ArrayList<String> temp = sets.get(fIndex);
				temp.addAll(sets.get(sIndex));
				sets.remove(Math.max(fIndex, sIndex));
				sets.remove(Math.min(sIndex, fIndex));
				sets.add(temp);
			}
		}
		for (int m = 0; m < mazeArray.length; m++) {
			for (int n = 0; n < mazeArray[0].length; n++) {
				if (mazeArray[m][n] == null) {
					mazeArray[m][n] = itemFactory.newInstance("brickWall");
				}
			}
		}
		addItems();

	}

	// testing...

	/*
	 * private void test() { endTime = System.currentTimeMillis(); //
	 * FieldsExtractor f = new FieldsExtractor(); // try { // f.extract(pacman);
	 * // } catch (IllegalAccessException | IllegalArgumentException |
	 * InvocationTargetException e) { // e.printStackTrace(); // } // while
	 * (f.hasNext()) { // System.out.print(f.nextName() + ": "); //
	 * System.out.println(f.nextValue()); // } System.out.println("That took " +
	 * (endTime - startTime) + " milliseconds"); for (int i = 0; i < sizeX; i++)
	 * { for (int j = 0; j < sizeY; j++) { if (mazeArray[i][j].getClass() !=
	 * BrickWall.class) { if (mazeArray[i][j].getClass() == Player.class) {
	 * System.out.print(" O "); } else if (mazeArray[i][j].getClass() ==
	 * BigSlowMonster.class) { System.out.print(" M "); } else if
	 * (mazeArray[i][j].getClass() == BulletsGift.class) {
	 * System.out.print(" B "); } else if (mazeArray[i][j].getClass() ==
	 * HealthGift.class) { System.out.print(" H "); } else if
	 * (mazeArray[i][j].getClass() == WoodWall.class) { System.out.print(" W ");
	 * } else if (mazeArray[i][j].getClass() == BigBomb.class) {
	 * System.out.print(" G "); } else if (mazeArray[i][j].getClass() ==
	 * SmallBomb.class) { System.out.print(" Q "); } else if
	 * (mazeArray[i][j].getClass() == SmallFastMonster.class) {
	 * System.out.print(" m "); } else { System.out.print("   "); } } else {
	 * System.out.print(" + "); } } System.out.print("\n"); } Scanner s = new
	 * Scanner(System.in); while (true) { String a = s.nextLine();
	 * this.moveRunner(a); if (a.contains("s")) { this.shoot(a); }
	 * this.UpdateMaze(); System.out.flush(); for (int i = 0; i < sizeX; i++) {
	 * for (int j = 0; j < sizeY; j++) { if (mazeArray[i][j].getClass() !=
	 * BrickWall.class) { if (mazeArray[i][j].getClass() == Player.class) {
	 * System.out.print(" O "); } else if (mazeArray[i][j].getClass() ==
	 * BigSlowMonster.class) { System.out.print(" M "); } else if
	 * (mazeArray[i][j].getClass() == BulletsGift.class) {
	 * System.out.print(" B "); } else if (mazeArray[i][j].getClass() ==
	 * HealthGift.class) { System.out.print(" H "); } else if
	 * (mazeArray[i][j].getClass() == WoodWall.class) { System.out.print(" W ");
	 * } else if (mazeArray[i][j].getClass() == BigBomb.class) {
	 * System.out.print(" G "); } else if (mazeArray[i][j].getClass() ==
	 * SmallBomb.class) { System.out.print(" Q "); } else if
	 * (mazeArray[i][j].getClass() == SmallFastMonster.class) {
	 * System.out.print(" m "); } else { System.out.print("   "); } } else {
	 * System.out.print(" + "); } } System.out.print("\n"); } } }
	 */

	public Item[][] getMazeArray() {
		return mazeArray;
	}

	private ArrayList<Point> BFS(Point mPosition, Point target) {
		Queue<Point> queue = new LinkedList<Point>();
		Set<Point> visitedNodes = new HashSet<Point>();
		Map<Point, Point> parentNodes = new HashMap<Point, Point>();
		queue.add(mPosition);
		parentNodes.put(mPosition, null);
		while (!queue.isEmpty()) {
			Point current = queue.poll();
			if (current == target) {
				break;
			}
			visitedNodes.add(current);
			int x = current.x;
			int y = current.y;
			if (x >= numberOfColumns || y >= numberOfRows) {
				continue;
			}
			if (!visitedNodes.contains(new Point(x, y + 1))
					&& (mazeArray[x][y + 1].getClass().getSuperclass() != Wall.class)
					&& (mazeArray[x][y + 1].getClass() != EndCell.class)) {
				queue.add(new Point(x, y + 1));
				parentNodes.put(new Point(x, y + 1), current);
			}
			if (!visitedNodes.contains(new Point(x + 1, y))
					&& (mazeArray[x + 1][y].getClass().getSuperclass() != Wall.class)
					&& (mazeArray[x + 1][y].getClass() != EndCell.class)) {
				queue.add(new Point(x + 1, y));
				parentNodes.put(new Point(x + 1, y), current);
			}
			if (!visitedNodes.contains(new Point(x, y - 1))
					&& (mazeArray[x][y - 1].getClass().getSuperclass() != Wall.class)
					&& (mazeArray[x][y - 1].getClass() != EndCell.class)) {
				queue.add(new Point(x, y - 1));
				parentNodes.put(new Point(x, y - 1), current);
			}
			if (!visitedNodes.contains(new Point(x - 1, y))
					&& (mazeArray[x - 1][y].getClass().getSuperclass() != Wall.class)
					&& (mazeArray[x - 1][y].getClass() != EndCell.class)) {
				queue.add(new Point(x - 1, y));
				parentNodes.put(new Point(x - 1, y), current);
			}
		}
		ArrayList<Point> shortestPath = new ArrayList<Point>();
		Point node = target;
		while (node != null) {
			shortestPath.add(node);
			node = parentNodes.get(node);
		}
		shortestPath.remove(shortestPath.size() - 1);
		return shortestPath;
	}

	public void UpdateMaze() {
		updates = (updates + 1) % 4;
		for (int i = 0; i < monsters.length; i++) {
			int x = monsters[i].x;
			int y = monsters[i].y;
			if (mazeArray[x][y].getClass().getSuperclass() != Monster.class) {
				for (int j = 0; j < hiddenItems.size(); j++) {
					Item s = hiddenItems.get(j);
					if (updates % ((Monster) s).getSpeed() != 0) {
						continue;
					}
					if (s.getPoistion().equals(monsters[i])) {
						if (((Monster) s).canHunt(runner.getPoistion())) {
							((Monster) s).setTarget(s.getPoistion());
							((Monster) s).setPath(BFS(s.getPoistion(), runner.getPoistion()));
						} else if (((Monster) s).newTarget()) {
							Point target = ((Monster) s).PossibleTarget(Math.min(numberOfRows, numberOfColumns) - 1);
							while (mazeArray[target.x][target.y].getClass() != EmptyCell.class) {
								target = ((Monster) s).PossibleTarget(Math.min(numberOfRows, numberOfColumns) - 1);
							}
							((Monster) s).setTarget(target);
							((Monster) s).setPath(BFS(((Monster) s).getPoistion(), target));
						}
						if(s.getClass().getSimpleName().equals("BigSlowMonster"))
							((BigSlowMonster) s).move();
							else if(s.getClass().getSimpleName().equals("SmallFastMonster"))
								((SmallFastMonster) s).move();
						int newX = ((Monster) s).getPoistion().x;
						int newY = ((Monster) s).getPoistion().y;
						if (mazeArray[newX][newY].getClass() == EmptyCell.class) {
							hiddenItems.remove(s);
							j--;
							mazeArray[newX][newY] = ((Monster) s);
						} else if (mazeArray[newX][newY].getClass() == Player.class) {
							runner.collision(s);
							lifeLostChecker();
						}
						monsters[i].x = newX;
						monsters[i].y = newY;

					}
				}
			} else {
				if (updates % (((Monster) mazeArray[x][y]).getSpeed()) != 0) {
					continue;
				}
				if (((Monster) mazeArray[x][y]).canHunt(runner.getPoistion())) {
					((Monster) mazeArray[x][y]).setTarget((mazeArray[x][y]).getPoistion());
					((Monster) mazeArray[x][y]).setPath(BFS((mazeArray[x][y]).getPoistion(), runner.getPoistion()));
				} else if (((Monster) mazeArray[x][y]).newTarget()) {
					Point target = ((Monster) mazeArray[x][y])
							.PossibleTarget(Math.min(numberOfRows, numberOfColumns) - 1);

					while (mazeArray[target.x][target.y].getClass() != EmptyCell.class) {
						target = ((Monster) mazeArray[x][y])
								.PossibleTarget(Math.min(numberOfRows, numberOfColumns) - 1);
					}
					((Monster) mazeArray[x][y]).setTarget(target);
					((Monster) mazeArray[x][y]).setPath(BFS((mazeArray[x][y]).getPoistion(), target));
				}
				if(mazeArray[x][y].getClass().getSimpleName().equals("BigSlowMonster"))
				((BigSlowMonster) mazeArray[x][y]).move();
				else if(mazeArray[x][y].getClass().getSimpleName().equals("SmallFastMonster"))
					((SmallFastMonster) mazeArray[x][y]).move();
				int newX = mazeArray[x][y].getPoistion().x;
				int newY = mazeArray[x][y].getPoistion().y;

				if (mazeArray[newX][newY].getClass() == Player.class) {
					runner.collision((Monster) mazeArray[x][y]);
					lifeLostChecker();
				}
				if (newX == x && newY == y) {
					((Monster) mazeArray[x][y]).initTarget();
					continue;
				} else if (mazeArray[newX][newY].getClass() != EmptyCell.class
						&& ((Monster) mazeArray[x][y]).getPathSize() != 0) {
					hiddenItems.add(mazeArray[x][y]);
				} else {
					mazeArray[newX][newY] = mazeArray[x][y];
				}
				mazeArray[x][y] = new EmptyCell();
				monsters[i].x = newX;
				monsters[i].y = newY;
			}
		}
	}

	/**
	 * moves the player with the given direction collecting gifts or stepping on
	 * bombs or facing death with the monsters.
	 * 
	 * @param direction
	 *            : u,d,r or l
	 */
	public void moveRunner(String direction) {
		LOGGER.info("Moving the runner...");
		Point playerPosition = runner.getPoistion();
		int x = playerPosition.x;
		int y = playerPosition.y;
		if (direction.equals("l")) {
			if (y > 0) {
				if (runner.collision(mazeArray[x][y - 1])) {
					runner.setPosition(new Point(x, y - 1));
					mazeArray[x][y - 1] = runner;
					mazeArray[x][y] = new EmptyCell();
				}
			}
		} else if (direction.equals("r")) {
			if (y < mazeArray[0].length - 1) {
				if (runner.collision(mazeArray[x][y + 1])) {
					runner.setPosition(new Point(x, y + 1));
					mazeArray[x][y + 1] = runner;
					mazeArray[x][y] = new EmptyCell();
				}
			}
		} else if (direction.equals("d")) {
			if (x < mazeArray.length - 1) {
				if (runner.collision(mazeArray[x + 1][y])) {
					runner.setPosition(new Point(x + 1, y));
					mazeArray[x + 1][y] = runner;
					mazeArray[x][y] = new EmptyCell();
				}
			}
		} else if (direction.equals("u")) {
			if (x > 0) {
				if (runner.collision(mazeArray[x - 1][y])) {
					runner.setPosition(new Point(x - 1, y));
					mazeArray[x - 1][y] = runner;
					mazeArray[x][y] = new EmptyCell();
				}
			}
		}
		lifeLostChecker();
	}

	/**
	 * shoots things.
	 * 
	 * @param direction
	 *            : sl,sr,su or sd
	 */
	public void shoot(String direction) {
		// Point playerPosition = runner.getPoistion();
		// int x = playerPosition.x;
		// int y = playerPosition.y;
		Bullet bullet = (Bullet) runner.shoot();
		// ArrayList<Point> bulletPath = new ArrayList<>();
		LOGGER.info("Shooting...");
		if (bullet != null) {
			myObserver.moveBullet(direction);
			// boolean hit = false;
			// while (!hit) {
			// if (direction.equals("sl")) {
			// if (y > 0) {
			// if (mazeArray[x][--y].getClass() != EmptyCell.class) {
			// if (bullet.collision(mazeArray[x][y])) {
			// myObserver.moveBullet(bulletPath);
			// mazeArray[x][y] = new EmptyCell();
			// }
			// hit = true;
			// } else {
			// bulletPath.add(new Point(x, y));
			// }
			// } else {
			// hit = true;
			// }
			// } else if (direction.equals("sr")) {
			// if (y < mazeArray[0].length - 1) {
			// if (mazeArray[x][++y].getClass() != EmptyCell.class) {
			// if (bullet.collision(mazeArray[x][y])) {
			// myObserver.moveBullet(bulletPath);
			// mazeArray[x][y] = new EmptyCell();
			// }
			// hit = true;
			// } else {
			// bulletPath.add(new Point(x, y));
			// }
			// } else {
			// hit = true;
			// }
			// } else if (direction.equals("sd")) {
			// if (x < mazeArray.length - 1) {
			// if (mazeArray[++x][y].getClass() != EmptyCell.class) {
			// if (bullet.collision(mazeArray[x][y])) {
			// myObserver.moveBullet(bulletPath);
			// mazeArray[x][y] = new EmptyCell();
			// }
			// hit = true;
			// } else {
			// bulletPath.add(new Point(x, y));
			// }
			// } else {
			// hit = true;
			// }
			// } else if (direction.equals("su")) {
			// if (x > 0) {
			// if (mazeArray[--x][y].getClass() != EmptyCell.class) {
			// if (bullet.collision(mazeArray[x][y])) {
			// myObserver.moveBullet(bulletPath);
			// mazeArray[x][y] = new EmptyCell();
			// }
			// hit = true;
			// } else {
			// bulletPath.add(new Point(x, y));
			// }
			// } else {
			// hit = true;
			// }
			// } else {
			// break;
			// }
			// }
		} else {
			System.out.println("Out of Bullets");
		}
	}

	private void lifeLostChecker() {
		if (lives > runner.getLives()) {
			mazeArray[runner.getPoistion().x][runner.getPoistion().y] = itemFactory.newInstance("emptyCell");
			runner.setPosition(new Point(1, 1));
			mazeArray[1][1] = runner;
			lives = runner.getLives();
		}
	}

	public void attachObserver(MazeObserver observer) {
		this.myObserver = observer;
	}

	public void notify(ActionEvent actionEvent) {
		this.myObserver.update(actionEvent);
	}

}