package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class SmallBomb  extends Bomb{

	// the amount of damage will be discussed.
		private final int SmallBombDamage;
		// icon path will be added by designer.
		//private final Icon SmallBombIcon = new ImageIcon("");
		
		private Image icon = new ImageIcon("./icon/fixed/bomb/bomb5.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);

		public SmallBomb(int difficulty) {
			if(difficulty == 1){
				SmallBombDamage = 15;
			} else if (difficulty == 2){
				SmallBombDamage = 20;
			} else {
				SmallBombDamage = 25;
			}
			setDamage(SmallBombDamage);
			setEffect();
			//setIcon(SmallBombIcon);
		}

		@Override
		public void setIcon(Image newIcon) {
			icon = newIcon;
		}

		@Override
		public Image getIcon() {
			return icon;
		}
}
