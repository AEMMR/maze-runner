package Model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import control.Direction;

public class SmallFastMonster extends Monster{

	private static Image icon;
	private static BufferedImage sprite;
	private final int width = 31;
	private final int height = 32;
	private static BufferedImage[] sprites;
	private int downCounter = 0; // to 2
	private int rightCounter = 6; // to 8
	private int upCounter = 9; // to 11
	private int leftCounter = 3; // to 5
	
	public SmallFastMonster(int difficulty) {
		super(difficulty);
		speed = (8 - difficulty) / 2;
		setSprite("./icon/animation/monster/monster0.png");
	}

	@Override
	public void setIcon(Image newIcon) {
		SmallFastMonster.icon = newIcon;
	}

	@Override
	public Image getIcon() {
		return SmallFastMonster.icon;
	}
	@SuppressWarnings("static-access")
	public void setSprite(String imagePath) {
		try {
			this.sprite = ImageIO.read(new File(imagePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		updateSprites();
		setIcon(sprites[0].getScaledInstance(20, 20, Image.SCALE_DEFAULT));
	}
	
	@SuppressWarnings("static-access")
	private void updateSprites() {
		int rows = 4;
		int cols = 3;
		sprites = new BufferedImage[rows * cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sprites[(i * cols) + j] = this.sprite
						.getSubimage(j * width, i * height, width, height);
			}
		}
	}
	@SuppressWarnings("static-access")
	public Image grapSprite(Direction direction) {
		int directionCode = direction.getCode();
		Image sprite;
		switch (directionCode) {
		// down (0 - 2)
		case 1:
			sprite = this.sprites[downCounter];
			if (downCounter == 2) {
				downCounter = 0;
			} else {
				downCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

		// right (6 - 8)
		case 2:
			sprite = this.sprites[rightCounter];
			if (rightCounter == 8) {
				rightCounter = 6;
			} else {
				rightCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

		// up (9 - 11)
		case 3:
			sprite = this.sprites[upCounter];
			if (upCounter == 11) {
				upCounter = 9;
			} else {
				upCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

		// left (3 - 5)
		case 4:
			sprite = this.sprites[leftCounter];
			if (leftCounter == 5) {
				leftCounter = 3;
			} else {
				leftCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);
		}
		return new BufferedImage(0, 0, 0); // empty image.
	}
	public void move() {
		if (currentPath.size() == 0) {
			target = this.getPoistion();
			return;
		}
		this.setIcon(grapSprite(findDirection(getPoistion(), currentPath.get(currentPath.size() - 1))));
		this.setPosition(currentPath.get(currentPath.size() - 1));
		currentPath.remove(currentPath.size() - 1);
	}
}
