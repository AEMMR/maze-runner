package Model;

abstract public class Wall extends Item{
	
	private boolean destructable;
	
	public boolean isDestructable() {
		return destructable;
	}

	public void setDestructable(boolean destructable) {
		this.destructable = destructable;
	}
	
	
}
