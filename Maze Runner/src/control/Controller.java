package control;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.logging.log4j.Logger;

import files.Load;
import files.Originator;
import files.Save;
import files.State;
import Model.BigSlowMonster;
import Model.BrickWall;
import Model.Item;
import Model.Maze;
import Model.Player;
import Model.SmallFastMonster;
import mazeGUI.EndGame;
import mazeGUI.HowToPlayCard;
import mazeGUI.LoadCard;
import mazeGUI.MainView;
import mazeGUI.NewGame;
import mazeGUI.ScoreBoard;
import mazeGUI.Settings;
import view.MazeObserver;

public class Controller {

	private static MainView game;
	private int difficulty = 2;
	private Maze maze;
	private Item[][] items;
	public static Controller controller;
	Timer t = new Timer();
	Thread timer;
	Originator originator;

	private static final Logger LOGGER = MyLogger.getInstance();

	public Item[][] getItems() {
		return items;
	}

	private SettingBuilder builder;
	private SettingImageFactory factory;
	ActionListener btnActionListener;

	public static void main(String[] args) {
		controller = new Controller();
	}

	public Controller() {
		LOGGER.info("Initializing the controller...");
		originator = new Originator();
		game = new MainView();
		game.gameplay = new NewGame();
		game.gameplay.setBackground(Color.WHITE);
		game.gameplay.setFocusable(true);
		game.scoreboard = new ScoreBoard();
		// game.scoreboard.setBackground(Color.PINK);
		game.setting = new Settings();
		game.setting.setBackground(new Color(176, 216, 218));
		game.howtoplay = new HowToPlayCard();
		game.loadgame = new LoadCard();
		game.endgame = new EndGame();
		builder = new SettingBuilder();
		factory = new SettingImageFactory(((Settings) game.setting));
		initializeGame();
		LOGGER.info("Setting game listeners...");
		setSpinnerListener();
		setButtonsListener();
		setNewGameListener();
		setSaveListener();
		setLoadListener();
		setSubmitListener();
	}

	private void setSaveListener() {
		game.save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String fileName = game.showDialog();
				if (fileName != null) {
					State state1 = new State();
					state1.setItems(items);
					state1.setTime(game.time.getText());
					state1.setScore(100);
					state1.setDifficulty(difficulty);
					state1.setBullets(Player.getInstance().getGun().getNumberOfBullets());
					state1.setHealth(Player.getInstance().getHealth());
					state1.setLives(Player.getInstance().getLives());
					originator.setState(state1);
					Save save = new Save(originator);
					save.save(fileName);
					game.loadgame = new LoadCard();
					game.gamePanel.add(game.loadgame, "loadgame");
				}
			}
		});

	}

	private void setButtonsListener() {
		btnActionListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (factory.getType(e)) {
				case "wall":
					builder.setWallIcon(factory.getSettingImage(e));
					break;
				case "bigMonster":
					builder.setBigMonsterPath(factory.getpath(e));
					break;
				case "smallMonster":
					builder.setSmallMonsterPath(factory.getpath(e));
					break;
				case "runner":
					builder.setRunnerPath(factory.getpath(e));
					// Player.getInstance().setSprite(factory.getpath(e));
					// builder.setRunnerIcon(factory.getSettingImage(e));
					break;
				}
			}
		};
		addListenerToButtons();
	}

	private void addListenerToButtons() {
		((Settings) game.setting).getWall1().addActionListener(btnActionListener);
		((Settings) game.setting).getWall2().addActionListener(btnActionListener);
		((Settings) game.setting).getWall3().addActionListener(btnActionListener);
		((Settings) game.setting).getMonster1().addActionListener(btnActionListener);
		((Settings) game.setting).getMonster2().addActionListener(btnActionListener);
		((Settings) game.setting).getMonster3().addActionListener(btnActionListener);
		((Settings) game.setting).getMonster4().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner1().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner2().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner3().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner4().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner5().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner6().addActionListener(btnActionListener);
		((Settings) game.setting).getRunner7().addActionListener(btnActionListener);
	}

	private void setNewGameListener() {
		game.newGame.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				game.switchCard("gameplay");
				game.back.setVisible(true);
				maze = new Maze(difficulty);
				maze.attachObserver(new MazeObserver(maze));

				items = maze.getMazeArray();
				((NewGame) (game.gameplay)).setItems(items);
				((NewGame) (game.gameplay)).setMaze(maze);
				Thread T = new Thread(new Runnable() {
					@SuppressWarnings("static-access")
					public void run() {
						while (game.card.equals("gameplay")) {
							if (maze.win()) {
								((EndGame) game.endgame).messageLabel.setText("Congratulations! :)");
								final String time = t.getTime();
								final String[] tokens = time.split(":");
								final int minutes = Integer.parseInt(tokens[0]);
								final int seconds = Integer.parseInt(tokens[1]);
								final int duration = minutes * 60 + seconds;
								((EndGame) game.endgame).scoreLabel
										.setText(String.valueOf(Player.getInstance().getScore() * 100 / duration));
								game.switchCard("endgame");
								if (timer != null && timer.isAlive()) {
									timer.stop();
								}
							}
							if (maze.lose()) {
								((EndGame) game.endgame).messageLabel.setText("Sorry! you lose. :(");
								final String time = t.getTime();
								final String[] tokens = time.split(":");
								final int minutes = Integer.parseInt(tokens[0]);
								final int seconds = Integer.parseInt(tokens[1]);
								final int duration = minutes * 60 + seconds;
								((EndGame) game.endgame).scoreLabel
										.setText(String.valueOf(Player.getInstance().getScore() * 100 / duration));
								game.switchCard("endgame");
								if (timer != null && timer.isAlive()) {
									timer.stop();
								}
							}
							try {
								Thread.currentThread().sleep(50);
							} catch (InterruptedException e) {
								LOGGER.warn("ERROR: Couldn't cease execution of time thread - line "
										+ Thread.currentThread().getStackTrace()[1].getLineNumber());
							}
						}

					}
				});
				T.start();
				LOGGER.info("Start calculating time.");
				game.gameTitle.setText("");
				game.displayToolPanel(true);
				game.save.setVisible(true);
				if (t != null) {
					t.resetTime();
				}
				if (timer != null && timer.isAlive()) {
					timer.stop();
				}
				// t = new Timer();
				timer = new Thread(t);
				timer.start();
				setIconsFromSetting();
			}
		});
	}

	private void setIconsFromSetting() {
		LOGGER.info("Setting selected icons...");
		BrickWall brickWall = new BrickWall();
		brickWall.setIcon(builder.getWallIcon());
		BigSlowMonster bigSlowMonster = new BigSlowMonster(difficulty);
		bigSlowMonster.setSprite(builder.getBigMonsterPath());
		SmallFastMonster smallFastMonster = new SmallFastMonster(difficulty);
		smallFastMonster.setSprite(builder.getSmallMonsterPath());
		Player.getInstance().setSprite(builder.getRunnerPath());
	}

	private void setSpinnerListener() {
		((Settings) game.setting).getSpinner().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				difficulty = ((Settings) game.setting).getDifficulty();
			}
		});
	}

	private void setLoadListener() {

		game.load.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {

				final String gameName = ((LoadCard) game.loadgame).getSelectedFile();
				game.load.setVisible(false);
				Load load = new Load(originator);
				load.load(gameName);
				game.back.setVisible(true);

				game.gameTitle.setText("");
				game.displayToolPanel(true);
				game.save.setVisible(true);
				game.switchCard("gameplay");
				difficulty = originator.getState().getDifficulty();
				maze = new Maze(difficulty);
				items = originator.getState().getItems();
				maze.setMazeArray(items);
				Player.getInstance().getGun().setNumberOfBullets(originator.getState().getBullets());
				Player.getInstance().setHealth(originator.getState().getHealth());
				Player.getInstance().setLives(originator.getState().getLives());
				Player.getInstance().setPosition(originator.getState().getPlayerPosition());
				final String time = originator.getState().getTime();
				final String tokens[] = time.split(":");
				final int minutes = Integer.parseInt(tokens[0]);
				final int seconds = Integer.parseInt(tokens[1]);
				if (t == null) {
					t = new Timer();
				}
				t.setMinutes(minutes);
				t.setSeconds(seconds);
				if (timer != null && timer.isAlive()) {
					timer.stop();
				}
				// t = new Timer();
				timer = new Thread(t);
				timer.start();
				maze.attachObserver(new MazeObserver(maze));
				((NewGame) (game.gameplay)).setItems(items);
				((NewGame) (game.gameplay)).setMaze(maze);

				setIconsFromSetting();
			}
		});
	}

	private void setSubmitListener() {
		((EndGame) (game.endgame)).submitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final String playerName = ((EndGame) (game.endgame)).nameField.getText();
				final int playerScore = Integer.parseInt(((EndGame) (game.endgame)).scoreLabel.getText());
				((ScoreBoard) game.scoreboard).addUser(playerName, playerScore);
				((ScoreBoard) game.scoreboard).saveScoreBoard();
				game.displayToolPanel(false);
				game.save.setVisible(false);
				game.scoreboard = new ScoreBoard();
				game.gamePanel.add(game.scoreboard, "scoreboard");
				game.switchCard("mainmenu");
			}
		});
	}

	public void updateToolPanel() {
		game.ammoCount.setText(String.valueOf(maze.runner.getGun().getNumberOfBullets()) + "x");
		game.healthCount.setText(String.valueOf(maze.runner.getHealth()));
		game.lifeCount.setText(String.valueOf(maze.runner.getLives()) + "x");
	}

	public static void updateTime(final String time) {
		game.time.setText(time);
	}

	private void initializeGame() {
		game.initialize();
	}
}
