package control;

public enum Direction {
	DOWN(1), RIGHT(2), UP(3), LEFT(4);
	private int code;
	Direction(int num) {
		this.code = num;
	}
	public int getCode() {
		return this.code;
	}
	
}
