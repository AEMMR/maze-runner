package control;

import java.awt.Image;

import javax.swing.ImageIcon;

public class SettingBuilder {

	private Image wallIcon = new ImageIcon("./icon/wall/wall0.png").getImage()
			.getScaledInstance(18, 18, Image.SCALE_DEFAULT);
	private Image smallMonsterIcon = new ImageIcon(
			"./icon/animation/monster/monster0.png").getImage()
			.getScaledInstance(18, 18, Image.SCALE_DEFAULT);;
	private Image bigMonsterIcon = new ImageIcon(
			"./icon/animation/monster/monster2.png").getImage()
			.getScaledInstance(18, 18, Image.SCALE_DEFAULT);;
	private Image runnerIcon = new ImageIcon(
			"./icon/animation/runner/runner0.png").getImage()
			.getScaledInstance(18, 18, Image.SCALE_DEFAULT);;
	private String bigMonsterPath = "./icon/animation/monster/monster2.png";
	private String smallMonsterPath = "./icon/animation/monster/monster0.png";
	private String runnerPath="./icon/animation/runner/runner0.png";
	
	public String getRunnerPath() {
		return runnerPath;
	}

	public String getBigMonsterPath() {
		return bigMonsterPath;
	}

	public void setBigMonsterPath(String bigMonsterPath) {
		this.bigMonsterPath = bigMonsterPath;
	}

	public String getSmallMonsterPath() {
		return smallMonsterPath;
	}

	public void setSmallMonsterPath(String smallMonsterPath) {
		this.smallMonsterPath = smallMonsterPath;
	}
	
	public Image getSmallMonsterIcon() {
		return smallMonsterIcon;
	}

	public void setSmallMonsterIcon(Image smallMonsterIcon) {
		this.smallMonsterIcon = smallMonsterIcon;
	}

	public Image getBigMonsterIcon() {
		return bigMonsterIcon;
	}

	public void setBigMonsterIcon(Image monsterIcon) {
		this.bigMonsterIcon = monsterIcon;
	}

	public Image getRunnerIcon() {
		return runnerIcon;
	}

	public void setRunnerIcon(Image runnerIcon) {
		this.runnerIcon = runnerIcon;
	}

	public Image getWallIcon() {
		return wallIcon;
	}

	public void setWallIcon(Image wall1Icon) {
		this.wallIcon = wall1Icon;
	}

	public void setRunnerPath(String getpath) {
		this.runnerPath=getpath;
		
	}
}
