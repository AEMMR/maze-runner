package control;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.ImageIcon;

import mazeGUI.Settings;

public class SettingImageFactory {
	private static final HashMap<Integer, Image> wallMap = new HashMap<Integer, Image>();
	private Settings setting;
	private String type;

	public SettingImageFactory(Settings settings) {
		setting = settings;
		type = new String();
	}

	public String getpath(ActionEvent e) {
		if (e.getSource() == setting.getMonster1()) {
			setting.highlightsmallMonster(setting.getMonster1());
			return "./icon/animation/monster/monster0.png";

		} else if (e.getSource() == setting.getMonster2()) {
			setting.highlightsmallMonster(setting.getMonster2());
			return "./icon/animation/monster/monster1.png";

		} else if (e.getSource() == setting.getMonster3()) {
			setting.highlightBigMonster(setting.getMonster3());
			return "./icon/animation/monster/monster2.png";

		} else if (e.getSource() == setting.getMonster4()) {
			setting.highlightBigMonster(setting.getMonster4());
			return "./icon/animation/monster/monster3.png";

		} else if (e.getSource() == setting.getRunner1()) {
			setting.highlightRunner(setting.getRunner1());
			return "./icon/animation/runner/runner0.png";

		} else if (e.getSource() == setting.getRunner2()) {
			setting.highlightRunner(setting.getRunner2());
			return "./icon/animation/runner/runner1.png";

		} else if (e.getSource() == setting.getRunner3()) {
			setting.highlightRunner(setting.getRunner3());
			return "./icon/animation/runner/runner2.png";

		} else if (e.getSource() == setting.getRunner4()) {
			setting.highlightRunner(setting.getRunner4());
			return "./icon/animation/runner/runner3.png";

		} else if (e.getSource() == setting.getRunner5()) {
			setting.highlightRunner(setting.getRunner5());
			return "./icon/animation/runner/runner4.png";
		} else if (e.getSource() == setting.getRunner6()) {
			setting.highlightRunner(setting.getRunner6());
			return "./icon/animation/runner/runner5.png";

		} else if (e.getSource() == setting.getRunner7()) {
			setting.highlightRunner(setting.getRunner7());
			return "./icon/animation/runner/runner6.png";

		}
		return null;
	}

	public String getType(ActionEvent e) {
		if (e.getSource() == setting.getWall1()) {
			type = "wall";
		} else if (e.getSource() == setting.getWall2()) {
			type = "wall";
		} else if (e.getSource() == setting.getWall3()) {
			type = "wall";
		} else if (e.getSource() == setting.getMonster1()) {
			type = "smallMonster";
		} else if (e.getSource() == setting.getMonster2()) {
			type = "smallMonster";

		} else if (e.getSource() == setting.getMonster3()) {
			type = "bigMonster";
		} else if (e.getSource() == setting.getMonster4()) {
			type = "bigMonster";
		} else if (e.getSource() == setting.getRunner1()) {
			type = "runner";
		} else if (e.getSource() == setting.getRunner2()) {
			type = "runner";

		} else if (e.getSource() == setting.getRunner3()) {
			type = "runner";
		} else if (e.getSource() == setting.getRunner4()) {
			type = "runner";
		} else if (e.getSource() == setting.getRunner5()) {
			type = "runner";

		} else if (e.getSource() == setting.getRunner6()) {
			type = "runner";
		} else if (e.getSource() == setting.getRunner7()) {
			type = "runner";

		}
		return type;
	}

	public Image getSettingImage(ActionEvent e) {
		if (e.getSource() == setting.getWall1()) {
			Image image = wallMap.get(1);
			if (image == null) {
				image = new ImageIcon("./icon/wall/wall0.png").getImage()
						.getScaledInstance(18, 18, Image.SCALE_DEFAULT);
				wallMap.put(1, image);
			}
			setting.highlightWall(setting.getWall1());
			return image;

		} else if (e.getSource() == setting.getWall2()) {
			Image image = wallMap.get(2);
			if (image == null) {
				image = new ImageIcon("./icon/wall/wall1.png").getImage()
						.getScaledInstance(18, 18, Image.SCALE_DEFAULT);
				wallMap.put(2, image);
			}
			setting.highlightWall(setting.getWall2());
			return image;
		} else if (e.getSource() == setting.getWall3()) {
			Image image = wallMap.get(3);
			if (image == null) {
				image = new ImageIcon("./icon/wall/wall2.png").getImage()
						.getScaledInstance(18, 18, Image.SCALE_DEFAULT);
				wallMap.put(3, image);
			}
			setting.highlightWall(setting.getWall3());
			return image;

		}// else if (e.getSource() == setting.getMonster1()){
			// setting.highlightsmallMonster(setting.getMonster1());
		// return (new ImageIcon("./icon/animation/monster/monster0.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getMonster2()){
		// setting.highlightsmallMonster(setting.getMonster2());
		// return (new ImageIcon("./icon/animation/monster/monster1.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getMonster3()){
		// setting.highlightBigMonster(setting.getMonster3());
		// return (new ImageIcon("./icon/animation/monster/monster2.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getMonster4()){
		// setting.highlightBigMonster(setting.getMonster4());
		// return (new ImageIcon("./icon/animation/monster/monster3.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getRunner1()){
		// setting.highlightRunner(setting.getRunner1());
		// return (new ImageIcon("./icon/animation/runner/runner0.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getRunner2()){
		// setting.highlightRunner(setting.getRunner2());
		// return (new ImageIcon("./icon/animation/runner/runner1.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getRunner3()){
		// setting.highlightRunner(setting.getRunner3());
		// return new ImageIcon("./icon/animation/runner/runner2.png")
		// .getImage();
		//
		// }else if (e.getSource() == setting.getRunner4()){
		// setting.highlightRunner(setting.getRunner4());
		// return (new ImageIcon("./icon/animation/runner/runner3.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getRunner5()){
		// setting.highlightRunner(setting.getRunner5());
		// return (new ImageIcon("./icon/animation/runner/runner4.png")
		// .getImage());
		// }else if (e.getSource() == setting.getRunner6()){
		// setting.highlightRunner(setting.getRunner6());
		// return (new ImageIcon("./icon/animation/runner/runner5.png")
		// .getImage());
		//
		// }else if (e.getSource() == setting.getRunner7()){
		// setting.highlightRunner(setting.getRunner7());
		// return (new
		// ImageIcon("./icon/animation/runner/runner6.png").getImage());
		//
		// }
		return null;
	}

}
