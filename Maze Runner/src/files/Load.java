package files;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import Model.Item;
import Model.ItemFactory;
import Model.Player;
import control.MyLogger;

public class Load {

	Originator originator;
	Item[][] arrayToLoad;
	String fileName;
	private int difficulty;
	private int health;
	private int lives;
	private int bullets;
	private ItemFactory itemFactory;
	State state;
	
	private static final Logger LOGGER = MyLogger.getInstance();

	public Load(Originator originator) {
		LOGGER.info("Loading game...");
		this.originator = originator;
		arrayToLoad = new Item[35][63];
		state = new State();
	}

	public void load(String fileName) {
		this.fileName = "./file/save/" + fileName + ".txt";
		loadFromFile();
		originator.setState(state);
		LOGGER.info("Game is loaded successfuly.");
	}

	private void loadFromFile() {

		String line = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			line = bufferedReader.readLine();
			line = line.substring(8, line.length());
			state.setScore(Integer.parseInt(line));
			line = bufferedReader.readLine();
			line = line.substring(13, line.length());
			this.difficulty = Integer.parseInt(line);
			line = bufferedReader.readLine();
			line = line.substring(9, line.length());
			this.health = Integer.parseInt(line);
			line = bufferedReader.readLine();
			line = line.substring(8, line.length());
			this.lives = Integer.parseInt(line);
			line = bufferedReader.readLine();
			line = line.substring(10, line.length());
			this.bullets = Integer.parseInt(line);
			line = bufferedReader.readLine();
			line = line.substring(7, line.length());
			state.setTime(line);
			state.setDifficulty(difficulty);
			state.setBullets(bullets);
			state.setHealth(health);
			state.setLives(lives);

			itemFactory = new ItemFactory(difficulty);
			for (int i = 0; i < 35; i++) {
				line = bufferedReader.readLine();
				for (int j = 0; j < line.length(); j++) {
					arrayToLoad[i][j] = setItemsArray(line.charAt(j), i, j);
				}
			}
			state.setItems(arrayToLoad);
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			LOGGER.warn("ERROR: Couldn't find the required game "+ fileName + " - line " 
					+ Thread.currentThread().getStackTrace()[1].getLineNumber());
		} catch (IOException ex) {
			LOGGER.warn("ERROR: Something went wrong while loading your game  "+ fileName + " - line " 
					+ Thread.currentThread().getStackTrace()[1].getLineNumber());
		}

	}

	private Item setItemsArray(char c, int i, int j) {
		switch (c) {
		case 'W':
			return itemFactory.newInstance("brickWall");
		case 'E':
			return itemFactory.newInstance("emptyCell");
		case 'U':
			return itemFactory.newInstance("bulletsGift");
		case 'O':
			return itemFactory.newInstance("bigBomb");
		case 'M':
			Item big = itemFactory.newInstance("bigSlowMonster");
			big.setPosition(new Point(i, j));
			return big;
		case 'H':
			return itemFactory.newInstance("healthGift");
		case 'P':
			Item player = itemFactory.newInstance("player");
			((Player) player).setHealth(health);
			((Player) player).setLives(lives);
			((Player) player).getGun().setNumberOfBullets(bullets);
			state.setPlayerPosition(new Point(i, j));
			player.setPosition(new Point(i, j));
			return player;
		case 'o':
			return itemFactory.newInstance("smallBomb");
		case 'm':
			Item small = itemFactory.newInstance("smallFastMonster");
			small.setPosition(new Point(i, j));
			return small;
		case 'w':
			return itemFactory.newInstance("woodWall");
		case 'F':
			return itemFactory.newInstance("endCell");

		}
		return null;
	}

}
