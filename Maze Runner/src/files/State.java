package files;

import java.awt.Point;

import Model.Item;

public class State {
	private Item[][] items;
	private int score;
	private int difficulty;
	private int health;
	private int lives;
	private String time;
	private int bullets;
	private Point playerPosition;
	
	public void setPlayerPosition(Point playerPosition) {
		this.playerPosition = playerPosition;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public int getBullets() {
		return bullets;
	}

	public void setBullets(int bullets) {
		this.bullets = bullets;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Item[][] getItems() {
		return items;
	}

	public void setItems(Item[][] items) {
		this.items = items;
	}

	public Point getPlayerPosition() {
		return playerPosition;
	}

}
