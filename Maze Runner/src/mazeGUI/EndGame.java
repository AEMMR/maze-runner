package mazeGUI;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

import org.apache.logging.log4j.Logger;

import control.MyLogger;

@SuppressWarnings("serial")
public class EndGame extends JPanel{
	
	public JLabel messageLabel;
	public JTextField nameField;
	public JButton submitBtn;
	public JLabel scoreLabel;
	
	private static final Logger LOGGER = MyLogger.getInstance();
	
	public EndGame() {
		LOGGER.info("Ending game...");
		setBackground(Color.ORANGE);
		initialize();
	}

	private void initialize() {
		setLayout(null);
		
		messageLabel = new JLabel("");
		messageLabel.setFont(new Font("Calibri", Font.PLAIN, 35));
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messageLabel.setBounds(53, 65, 387, 50);
		add(messageLabel);
		
		JLabel lblYourScoreIs = new JLabel("Your Score is");
		lblYourScoreIs.setHorizontalAlignment(SwingConstants.CENTER);
		lblYourScoreIs.setFont(new Font("Calibri", Font.BOLD, 35));
		lblYourScoreIs.setBounds(53, 126, 256, 50);
		add(lblYourScoreIs);
		
		scoreLabel = new JLabel("");
		scoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
		scoreLabel.setFont(new Font("Calibri", Font.BOLD, 35));
		scoreLabel.setBounds(282, 126, 119, 50);
		add(scoreLabel);
		
		JLabel labelExclamation = new JLabel("!");
		labelExclamation.setHorizontalAlignment(SwingConstants.CENTER);
		labelExclamation.setFont(new Font("Calibri", Font.PLAIN, 35));
		labelExclamation.setBounds(400, 129, 27, 44);
		add(labelExclamation);
		
		JLabel lblEnterYourName = new JLabel("Enter your Name");
		lblEnterYourName.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterYourName.setFont(new Font("Calibri", Font.BOLD, 14));
		lblEnterYourName.setBounds(79, 189, 119, 50);
		add(lblEnterYourName);
		
		nameField = new JTextField();
		nameField.setBorder(new LineBorder(Color.BLACK));
		nameField.setHorizontalAlignment(SwingConstants.CENTER);
		nameField.setBounds(208, 204, 135, 20);
		add(nameField);
		nameField.setColumns(10);
		
		submitBtn = new JButton("Submit");
		submitBtn.setBackground(Color.WHITE);
		submitBtn.setBorder(new LineBorder(new Color(0, 0, 0)));
		submitBtn.setBounds(351, 203, 89, 23);
		add(submitBtn);
	}
}
