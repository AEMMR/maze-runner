package mazeGUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Cursor;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.ColorUIResource;
import javax.swing.SwingConstants;


public class MainView {
	
	public JFrame frame;
	public String card;
	
	/* tools */
	public JPanel toolPanel;
	private JButton resizeScreen;
	private JButton exitScreen;
	public JLabel gameTitle;
	private boolean isFullscreen = true;
	private final Dimension buttonDimension = new Dimension(34, 34);
	public JLabel time, ammoCount, healthCount, lifeCount;
	public JButton timer, ammo, health, life, save, load;
	
	/* images */
	private BufferedImage backgroundImage;
	private final Icon fullscreen = new ImageIcon("./icon/fullscreen.png");
	private final Icon exitfullscreen = new ImageIcon("./icon/exitfullscreen.png");
	private final Icon exitgame = new ImageIcon("./icon/exitgame.png");
	private final Icon backIcon = new ImageIcon("./icon/back.png");
	private final Icon ammoIcon = new ImageIcon("./icon/fixed/gift/ammo.png");
	private final Icon healthIcon = new ImageIcon("./icon/fixed/gift/health.png");
	private final Icon lifeIcon = new ImageIcon("./icon/fixed/gift/life.png");
	private final Icon timerIcon = new ImageIcon("./icon/timer.png");
	private final Icon saveIcon = new ImageIcon("./icon/save.png");
	private final Icon loadIcon = new ImageIcon("./icon/load.png");
	
	
	/* maze dimensions */
	private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private final double mazeRatio = 16D / 9D;
	private final int gamePanelHeight = (int) screenSize.getHeight() - 100;
	private final int gamePanelWidth = (int) (gamePanelHeight * mazeRatio);
	private final int gamePanelX = (int) (screenSize.getWidth() - gamePanelWidth) / 2;
	private final int gamePanelY = (int) (screenSize.getHeight() - gamePanelHeight) / 2;
	
	/* cards */
	public JPanel gamePanel, gameplay, scoreboard, loadgame, endgame;
	private JPanel mainmenu;
	public JPanel setting, howtoplay;
	private JPanel buttonsPanel;
	
	/* buttons */
	public JButton newGame, scoreBoard;
	private JButton howToPlay, settings, loadGame, exitGame;
	public JButton getLoadGame() {
		return loadGame;
	}

	public JButton back;
	
	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	public void initialize() {
		setFrame();
		customizeTooltip();
		
		setToolPanel();
		frame.getContentPane().add(toolPanel);
		setExitButton();
		setResizeButton();
		setBackButton();
		
		
		setGamePanel();
		setBackgroundImage();
		setCards();
		setButtonsPanel();
		setGameTitle();
		frame.getContentPane().add(gamePanel);
		
		setSave();
		setLoad();
		setAmmo();
		setHealth();
		setLife();
		setTime();
		
		frame.setVisible(true);
	}
	
	private void setLoad() {
		load = new JButton();
		load.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		load.setBorder(new LineBorder(Color.BLACK, 1));
		load.setIcon(loadIcon);
		load.setBackground(null);
		load.setBounds(buttonDimension.width * 3, 0,
				buttonDimension.width, buttonDimension.height);
		load.setToolTipText("Load Game");
		
		load.setVisible(false);
		toolPanel.add(load);
	}
	
	private void setAmmo() {
		ammo = new JButton();
		ammo.setBorder(new LineBorder(Color.BLACK, 1));
		ammo.setIcon(ammoIcon);
		ammo.setBackground(null);
		ammo.setBounds(gamePanelWidth - buttonDimension.width, 0,
				buttonDimension.width, buttonDimension.height);
		
		
		ammoCount = new JLabel("6x");
		ammoCount.setFont(new Font("Calibri", Font.BOLD, 22));
		ammoCount.setForeground(Color.WHITE);
		ammoCount.setBounds(gamePanelWidth - buttonDimension.width * 2 + 13, 5,
				2 * 15, buttonDimension.height);
		
		ammo.setVisible(false);
		ammoCount.setVisible(false);
		
		toolPanel.add(ammo);
		toolPanel.add(ammoCount);
	}
	
	private void setHealth() {
		health = new JButton();
		health.setBorder(new LineBorder(Color.BLACK, 1));
		health.setIcon(healthIcon);
		health.setBackground(null);
		health.setBounds(gamePanelWidth - buttonDimension.width * 3, 0,
				buttonDimension.width, buttonDimension.height);
		
		
		healthCount = new JLabel("100");
		healthCount.setHorizontalAlignment(SwingConstants.RIGHT);
		healthCount.setFont(new Font("Calibri", Font.BOLD, 22));
		healthCount.setForeground(Color.WHITE);
		healthCount.setBounds(gamePanelWidth - buttonDimension.width * 4 - 13, 5,
				3 * 15, buttonDimension.height);
		
		health.setVisible(false);
		healthCount.setVisible(false);
		
		toolPanel.add(health);
		toolPanel.add(healthCount);
	}
	
	private void setLife() {
		life = new JButton();
		life.setBorder(new LineBorder(Color.BLACK, 1));
		life.setIcon(lifeIcon);
		life.setBackground(null);
		life.setBounds(gamePanelWidth - buttonDimension.width * 6 + 20, 0,
				buttonDimension.width, buttonDimension.height);
		
		
		lifeCount = new JLabel("3x");
		lifeCount.setHorizontalAlignment(SwingConstants.RIGHT);
		lifeCount.setFont(new Font("Calibri", Font.BOLD, 22));
		lifeCount.setForeground(Color.WHITE);
		lifeCount.setBounds(gamePanelWidth - buttonDimension.width * 6 - 10, 5,
				2 * 15, buttonDimension.height);
		
		life.setVisible(false);
		lifeCount.setVisible(false);
		
		toolPanel.add(life);
		toolPanel.add(lifeCount);
	}
	
	private void setSave() {
		save = new JButton();
		save.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		save.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),
                "none");
		save.setBorder(new LineBorder(Color.BLACK, 1));
		save.setIcon(saveIcon);
		save.setBackground(null);
		save.setBounds(buttonDimension.width * 3, 0,
				buttonDimension.width, buttonDimension.height);
		save.setToolTipText("Save Game");
		
		save.setVisible(false);
		toolPanel.add(save);
	}
	
	private void setTime() {
		timer = new JButton();
		timer.setBorder(new LineBorder(Color.BLACK, 1));
		timer.setIcon(timerIcon);
		timer.setBackground(null);
		timer.setBounds(gamePanelWidth - buttonDimension.width * 7 - 10, 0,
				buttonDimension.width, buttonDimension.height);
		
		
		time = new JLabel("00:00");
		time.setHorizontalAlignment(SwingConstants.RIGHT);
		time.setFont(new Font("Calibri", Font.BOLD, 22));
		time.setForeground(Color.WHITE);
		time.setBounds(gamePanelWidth - buttonDimension.width * 7 - 90, 5,
				5 * 15, buttonDimension.height);
		
		timer.setVisible(false);
		time.setVisible(false);
		
		toolPanel.add(timer);
		toolPanel.add(time);
	}
	
	private void setToolPanel() {
		toolPanel = new JPanel();
		toolPanel.setLayout(null);
		toolPanel.setBounds(gamePanelX, gamePanelY - buttonDimension.height,
									gamePanelWidth, buttonDimension.height);
		toolPanel.setBackground(Color.BLACK);
	}
	
	/**
	 * go to full screen mode.
	 */
	private void toFullScreen() {
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setVisible(false);
		isFullscreen = true;
		resizeScreen.setIcon(exitfullscreen);
		toolPanel.setBounds(gamePanelX, gamePanelY - buttonDimension.height,
									gamePanelWidth, buttonDimension.height);
		
		gamePanel.setBounds(gamePanelX, gamePanelY, gamePanelWidth, gamePanelHeight);
		resizeScreen.setToolTipText("Exit Full Screen");
		frame.setVisible(true);
	}
	
	/**
	 * exit full screen mode.
	 */
	private void exitFullScreen() {
		frame.setVisible(false);
		isFullscreen = false;
		resizeScreen.setIcon(fullscreen);
		frame.setBounds(gamePanelX, (int) (gamePanelY - buttonDimension.getHeight()),
				gamePanelWidth, (int) (gamePanelHeight + buttonDimension.getWidth()));
		
		toolPanel.setBounds(0, 0, gamePanelWidth, buttonDimension.height);
		gamePanel.setBounds(0, (int) buttonDimension.getHeight(),
								gamePanelWidth, gamePanelHeight);
		resizeScreen.setToolTipText("Full Screen");
		frame.setVisible(true);
	}
	
	void customizeTooltip() {
		UIManager.put("ToolTip.background", new ColorUIResource(255, 247, 200));
		Border border = BorderFactory.createLineBorder(new Color(76, 79, 83));
		UIManager.put("ToolTip.border", border);
		ToolTipManager.sharedInstance().setDismissDelay(3000);
	}
	
	void setFrame() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("./icon/logo.png"));
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setUndecorated(true);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	void setGamePanel() {
		gamePanel = new JPanel();
		gamePanel.setLayout(new CardLayout());
		gamePanel.setBackground(Color.WHITE);
		gamePanel.setBounds(gamePanelX, gamePanelY, gamePanelWidth, gamePanelHeight);
		gamePanel.setBorder(new LineBorder(Color.BLACK, 2));
	}
	
	@SuppressWarnings("serial")
	void setCards() {
		mainmenu = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				g.drawImage(backgroundImage.getScaledInstance(gamePanelWidth,
							gamePanelHeight, Image.SCALE_DEFAULT), 0, 0, null);
			}
		};
		mainmenu.setLayout(null);

		gamePanel.add(mainmenu, "mainmenu");
		gamePanel.add(gameplay, "gameplay");
		gamePanel.add(scoreboard, "scoreboard");
		gamePanel.add(howtoplay, "howtoplay");
		gamePanel.add(setting, "setting");
		gamePanel.add(loadgame, "loadgame");
		gamePanel.add(endgame, "endgame");
	}
	
	void setButtonsPanel() {
		final Font font = new Font("Calibri", Font.BOLD, 22);
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new GridLayout(6, 1));
		final int buttonsPanelHeight = 300, buttonsPanelWidth = 400;
		buttonsPanel.setBounds((gamePanelWidth - buttonsPanelWidth) / 2,
							(gamePanelHeight - buttonsPanelHeight) / 2 + 150,
							buttonsPanelWidth, buttonsPanelHeight);
		//new game
		newGame = new JButton("New Game");
		newGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		newGame.setFont(font);
		newGame.setBackground(Color.BLACK);
		newGame.setForeground(Color.WHITE);
		//score board
		scoreBoard = new JButton("Score Board");
		scoreBoard.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		scoreBoard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchCard("scoreboard");
				gameTitle.setText("Score Board");
				back.setVisible(true);
			}
		});
		scoreBoard.setFont(font);
		scoreBoard.setBackground(Color.BLACK);
		scoreBoard.setForeground(Color.WHITE);
		//how to play
		howToPlay = new JButton("How To Play");
		howToPlay.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		howToPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchCard("howtoplay");
				gameTitle.setText("How To Play");
				back.setVisible(true);
			}
		});
		howToPlay.setFont(font);
		howToPlay.setBackground(Color.BLACK);
		howToPlay.setForeground(Color.WHITE);
		// settings
		settings = new JButton("Settings");
		settings.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		settings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchCard("setting");
				gameTitle.setText("Settings");
				back.setVisible(true);
			}
		});
		settings.setFont(font);
		settings.setBackground(Color.BLACK);
		settings.setForeground(Color.WHITE);
		//load game
		loadGame = new JButton("Load");
		loadGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		loadGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				load.setVisible(true);
				switchCard("loadgame");
				gameTitle.setText("Load Game");
				back.setVisible(true);
			}
		});
		loadGame.setFont(font);
		loadGame.setBackground(Color.BLACK);
		loadGame.setForeground(Color.WHITE);
		//exit game
		exitGame = new JButton("Exit Game");
		exitGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		exitGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exitGame();
			}
		});
		exitGame.setFont(font);
		exitGame.setBackground(Color.BLACK);
		exitGame.setForeground(Color.WHITE);
		
		buttonsPanel.add(newGame);
		buttonsPanel.add(scoreBoard);
		buttonsPanel.add(howToPlay);
		buttonsPanel.add(settings);
		buttonsPanel.add(loadGame);
		buttonsPanel.add(exitGame);
		mainmenu.add(buttonsPanel, BorderLayout.CENTER);
	}
	
	void setExitButton() {
		exitScreen = new JButton();
		exitScreen.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		exitScreen.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),
                "none");
		exitScreen.setToolTipText("Exit");
		exitScreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exitGame();
			}
		});
		exitScreen.setBorder(new LineBorder(Color.BLACK, 1));
		exitScreen.setIcon(exitgame);
		exitScreen.setBackground(null);
		exitScreen.setBounds(0, 0,
				(int) buttonDimension.getWidth(), (int) buttonDimension.getHeight());
		toolPanel.add(exitScreen);
	}
	
	void setResizeButton() {
		resizeScreen = new JButton();
		resizeScreen.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		resizeScreen.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),
                "none");
		resizeScreen.setToolTipText("Exit Full Screen");
		resizeScreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (isFullscreen) {
					exitFullScreen();
				} else {
					toFullScreen();
				}
			}
		});
		resizeScreen.setBorder(new LineBorder(Color.BLACK, 1));
		resizeScreen.setIcon(exitfullscreen);
		resizeScreen.setBackground(null);
		resizeScreen.setBounds((int) buttonDimension.getWidth(), 0,
					(int) buttonDimension.getWidth(), (int) buttonDimension.getHeight());
		toolPanel.add(resizeScreen);
	}
	
	void setBackButton() {
		back = new JButton();
		back.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		back.getInputMap().put(KeyStroke.getKeyStroke("SPACE"),
                "none");
		back.setToolTipText("Back To Main Menu");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (card.equals("gameplay")) {
					/* show dialogue to ask if user wants to save the game no 1 yes 0 cancel2 */
					int answer=JOptionPane.showConfirmDialog(frame, "Do you want save your game ?");
					if(answer==0){
						save.doClick();
						displayToolPanel(false);
						switchCard("mainmenu");
						gameTitle.setText("Maze Runner");
						back.setVisible(false);
						save.setVisible(false);
					}else if(answer==1){
						displayToolPanel(false);
						switchCard("mainmenu");
						gameTitle.setText("Maze Runner");
						back.setVisible(false);
						save.setVisible(false);
					
					}
				}else {
				displayToolPanel(false);
				switchCard("mainmenu");
				gameTitle.setText("Maze Runner");
				back.setVisible(false);
				save.setVisible(false);
				load.setVisible(false);
			}
			}
		});
		back.setBorder(new LineBorder(Color.BLACK, 1));
		back.setIcon(backIcon);
		back.setBackground(null);
		back.setBounds((int) buttonDimension.getWidth() * 2, 0,
				(int) buttonDimension.getWidth(), (int) buttonDimension.getHeight());
		back.setVisible(false);
		toolPanel.add(back);
	}
	
	void setGameTitle() {
		gameTitle = new JLabel("Maze Runner");
		gameTitle.setFont(new Font("Calibri", Font.BOLD, 18));
		gameTitle.setForeground(Color.WHITE);
		gameTitle.setBounds((gamePanelWidth - 105) / 2, 0,
					105, (int) buttonDimension.getHeight());
		toolPanel.add(gameTitle);
	}
	
	/**
	 * exit game.
	 */
	private void exitGame() {
		frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	}
	
	/**
	 * load background image.
	 */
	void setBackgroundImage() {
		try {
			backgroundImage = ImageIO.read(new File("./icon/background.jpg"));
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public void displayToolPanel(final boolean x) {
		ammo.setVisible(x);
		ammoCount.setVisible(x);
		health.setVisible(x);
		healthCount.setVisible(x);
		life.setVisible(x);
		lifeCount.setVisible(x);
		timer.setVisible(x);
		time.setVisible(x);
	}
	
	/**
	 * switch between main menu and game play.
	 */
	public void switchCard(final String cardName) {
		CardLayout c = (CardLayout)(gamePanel.getLayout());
		c.show(gamePanel, cardName);
		setCardName(cardName);
	}
	
	public void setCardName(final String cardName) {
		card = cardName;
	}

	public String showDialog() {
		String name = JOptionPane.showInputDialog(frame, "What's your game name?");
		return name;
	}
}
