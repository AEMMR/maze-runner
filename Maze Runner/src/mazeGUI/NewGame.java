package mazeGUI;

import java.awt.Dimension;
import java.awt.Graphics;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import Model.Item;
import Model.Maze;

import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class NewGame extends JPanel{
	
	final int rows = 36, columns = 64;
	private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private final double mazeRatio = 16D / 9D;
	private final int gamePanelHeight = (int) screenSize.getHeight() - 100;
	private final int gamePanelWidth = (int) (gamePanelHeight * mazeRatio);
	private final int iconWidth = gamePanelWidth / columns;
	private final int vmargin = (gamePanelHeight - iconWidth * rows) / 2 + iconWidth / 2;
	private final int hmargin = (gamePanelWidth - iconWidth * columns) / 2 + iconWidth / 2;
	private Item[][] items;
	private Maze mMaze;
	
	public NewGame() {
		setKeyBindings();
	}
	
	private void setKeyBindings() {
		ActionMap actionMap = getActionMap();
		InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		final String vkLeft = "VK_LEFT";
		final String vkRight = "VK_RIGHT";
		final String vkUp = "VK_UP";
		final String vkDown = "VK_DOWN";
		final String spacebar = "VK_SPACE";
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), vkLeft);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), vkRight);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), vkUp);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), vkDown);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), spacebar);
		
		actionMap.put(vkLeft, new KeyAction(vkLeft));
		actionMap.put(vkRight, new KeyAction(vkRight));
		actionMap.put(vkUp, new KeyAction(vkUp));
		actionMap.put(vkDown, new KeyAction(vkDown));
		actionMap.put(spacebar, new KeyAction(spacebar));
	}
	
	private class KeyAction extends AbstractAction {
		public KeyAction(String actionCommand) {
			putValue(ACTION_COMMAND_KEY, actionCommand);
		}
		@Override
		public void actionPerformed(ActionEvent actionEvt) {
			mMaze.notify(actionEvt);
    	  	repaint();
		}
	}

	public void setItems(Item[][] items) {
		this.items = items;
		repaint();
	}

	public void setMaze(Maze maze) {
		this.mMaze = maze;
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		for (int x = 0; x < items.length; x++) {
			for (int y = 0; y < items[0].length; y++) {
				g.drawImage(items[x][y].getIcon(), y * iconWidth + hmargin, x * iconWidth + vmargin, null);
			}
		}
		repaint();
	}
}
