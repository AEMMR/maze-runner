package mazeGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.apache.logging.log4j.Logger;

import control.MyLogger;

@SuppressWarnings("serial")
public class ScoreBoard extends JPanel{
	
	private final String filePath = "./file/scoreboard.txt";
	private Scanner scan;
	private ArrayList<User> users = new ArrayList<User>();
	
	private static final Logger LOGGER = MyLogger.getInstance();
	
	public ScoreBoard() {
		LOGGER.info("Loading score board...");
		openFile(filePath);
		readFile();
		closeFile();
		initialize();
	}
	
	private void openFile(final String path) {
		try {
			scan = new Scanner(new File(path));
		} catch (FileNotFoundException e) {
			LOGGER.error("Error loading score board!");
		}
	}

	private void readFile() {
		String tempLine;
		String name;
		int score = 0;
		while(scan.hasNext()){
			tempLine = scan.nextLine();
			name = tempLine.replaceAll("[0-9]", "").trim();
			try {
				score = Integer.parseInt(tempLine.replaceAll("[\\D]", "").trim());
			} catch (Exception e) {
				LOGGER.error("Error reading score!");
			}
			users.add(new User(name, score));
		}
	}
	
	private void closeFile() {
		scan.close();	
	}
	
	private void initialize() {
		setLayout(new BorderLayout());
		String[] columnNames = {"Rank", "Name", "Score"};
		String[][] data = new String[users.size()][columnNames.length];
		int i = 0;
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User current = iterator.next();
			data[i][0] = String.valueOf(i + 1);
			data[i][1] = current.getName();
			data[i][2] = String.valueOf(current.getScore());
			i++;
		}
		DefaultTableModel model = new DefaultTableModel(data, columnNames);
		JTable table = setTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		JScrollBar bar = scrollPane.getVerticalScrollBar();
		bar.setPreferredSize(new Dimension(10, 0));
		scrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
		scrollPane.setBackground(Color.BLACK);
		add(scrollPane, BorderLayout.CENTER);
	}
	
	private JTable setTable(DefaultTableModel model) {
		JTable table = new JTable(model) {
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				if (row == 0) {
					c.setBackground(new Color(255,215,0));
				}
				else if (!isRowSelected(row)) {
					c.setBackground(row % 2 == 0 ? getBackground() : Color.LIGHT_GRAY);
				}
				return c;
			}
		};
		setCellsAlignment(table, SwingConstants.CENTER);
		JTableHeader th = table.getTableHeader();
		th.setBackground(Color.BLACK);
		th.setForeground(Color.WHITE);
		th.setFont(new Font("Calibri", Font.BOLD, 20));
		table.setBackground(Color.PINK);
		table.setFont(new Font("Calibri", Font.BOLD, 18));
		table.setRowHeight(30);
		table.setEnabled(false);
		table.setShowHorizontalLines(false);
		return table;
	}
	
	private void setCellsAlignment(JTable table, int alignment){
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(alignment);
        TableModel tableModel = table.getModel();
        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
            table.getColumnModel().getColumn(columnIndex).setCellRenderer(rightRenderer);
        }
    }
	
	public static class MyScrollBarUI extends BasicScrollBarUI {
		private final Dimension d = new Dimension();
		
		@Override
		protected JButton createDecreaseButton(int orientation) {
			return new JButton() {
				@Override
				public Dimension getPreferredSize() {
					return d;
				}
			};
		}
		
		@Override
		protected JButton createIncreaseButton(int orientation) {
			return new JButton() {
				@Override
				public Dimension getPreferredSize() {
					return d;
				}
			};
		}
		
		@Override
		protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
			Graphics2D g2 = (Graphics2D) g.create();
		    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		    Color color = Color.BLACK;
		    JScrollBar sb = (JScrollBar) c;
		    if (!sb.isEnabled() || r.width > r.height) {
		    	return;
		    }
		    g2.setPaint(color);
		    g2.fillRoundRect(r.x, r.y, r.width, r.height, 10, 10);
		    g2.drawRoundRect(r.x, r.y, r.width, r.height, 10, 10);
		    g2.dispose();
		}
		@Override
		protected void setThumbBounds(int x, int y, int width, int height) {
			super.setThumbBounds(x, y, width, height);
			scrollbar.repaint();
		}
	}
	
	public void addUser(final String name, final int score) {
		User newUser = new User(name.trim().replaceAll(" +", " "), score);
		ListIterator<User> iterator = users.listIterator();
		while(iterator.hasNext()) {
			if (score >= iterator.next().getScore()) {
				iterator.previous();
				iterator.add(newUser);
				return;
			}
		}
		iterator.add(newUser);
	}
	
	public void saveScoreBoard() {
		File file = new File(filePath);
		try {
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			FileWriter fileWriter = new FileWriter(file, false);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			Iterator<User> iterator = users.iterator();
			while (iterator.hasNext()) {
				User current = iterator.next();
				bufferedWriter.write(current.getName() + " " +
						String.valueOf(current.getScore()) + "\r\n");
			}
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException ioe) {
			System.out.println("Error saving score board!");
		}
	}
}
