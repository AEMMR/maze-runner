package mazeGUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;

import org.apache.logging.log4j.Logger;

import control.MyLogger;

@SuppressWarnings("serial")
public class Settings extends JPanel {
	private JSpinner spinner;
	SpinnerListModel m_listSpinnerModel;

	private ArrayList<JButton> wallsList;
	private ArrayList<JButton> bigMonstersList;
	private ArrayList<JButton> smallMontersList;
	private ArrayList<JButton> runnersList;
	private JButton monster1, monster2;
	private JButton monster3;
	private JButton monster4;
	private JButton runner1;
	private JButton runner2;
	private JButton runner3;
	private JButton runner4;
	private JButton runner5;
	private JButton runner6;
	private JButton runner7;
	private JButton wall1;
	private JButton wall2;
	private JButton wall3;
	
	private static final Logger LOGGER = MyLogger.getInstance();

	public JButton getMonster1() {
		return monster1;
	}

	public JButton getMonster2() {
		return monster2;
	}

	public JButton getMonster3() {
		return monster3;
	}

	public JButton getMonster4() {
		return monster4;
	}

	public JButton getRunner1() {
		return runner1;
	}

	public JButton getRunner2() {
		return runner2;
	}

	public JButton getRunner3() {
		return runner3;
	}

	public JButton getRunner4() {
		return runner4;
	}

	public JButton getRunner5() {
		return runner5;
	}

	public JButton getRunner6() {
		return runner6;
	}

	public JButton getRunner7() {
		return runner7;
	}

	public JButton getWall1() {
		return wall1;
	}

	public JButton getWall2() {
		return wall2;
	}

	public JButton getWall3() {
		return wall3;
	}

	public JSpinner getSpinner() {
		return spinner;
	}

	public Settings() {
		LOGGER.info("Displaying settings card..");
		wallsList = new ArrayList<JButton>();
		smallMontersList = new ArrayList<JButton>();
		bigMonstersList = new ArrayList<JButton>();
		runnersList = new ArrayList<JButton>();
		initialize();
	}

	private void initialize() {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		displayDiffcultySpinner();
		displayWallChooser();
		displayRunnerChooser();
		displayBigMonsterChooser();
		displaySmallMonsterChooser();
	}

	private void displaySmallMonsterChooser() {
		JPanel smallMonsterPanel = new JPanel();
		smallMonsterPanel.setBackground(new Color(176, 216, 218));
		smallMonsterPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel label = new JLabel("Choose your first monster : ");
		label.setFont(new Font("Serif", Font.BOLD, 40));
		smallMonsterPanel.add(label);
		monster1 = new JButton();
		BufferedImage BI;
		try {
			BI = ImageIO
					.read(new File("./icon/animation/monster/monster0.png"));
			Image newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40,
					40, Image.SCALE_DEFAULT);
			monster1.setIcon(new ImageIcon(newimg));
			monster1.setBackground(Color.BLACK);
			smallMontersList.add(monster1);
			smallMonsterPanel.add(monster1);
			monster2 = new JButton();
			BI = ImageIO
					.read(new File("./icon/animation/monster/monster1.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			monster2.setIcon(new ImageIcon(newimg));
			monster2.setBackground(Color.BLACK);
			smallMontersList.add(monster2);
			smallMonsterPanel.add(monster2);
		} catch (IOException e) {
			LOGGER.error("IOException in line " + Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
		add(smallMonsterPanel);

	}

	private void displayBigMonsterChooser() {
		JPanel monsterPanel = new JPanel();
		monsterPanel.setBackground(new Color(176, 216, 218));
		monsterPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel label = new JLabel("Choose your second monster : ");
		label.setFont(new Font("Serif", Font.BOLD, 40));
		monsterPanel.add(label);

		BufferedImage BI;
		try {
			monster3 = new JButton();
			BI = ImageIO
					.read(new File("./icon/animation/monster/monster2.png"));
			Image newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40,
					40, Image.SCALE_DEFAULT);
			monster3.setIcon(new ImageIcon(newimg));
			monster3.setBackground(Color.BLACK);
			bigMonstersList.add(monster3);
			monsterPanel.add(monster3);
			monster4 = new JButton();
			BI = ImageIO
					.read(new File("./icon/animation/monster/monster3.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			monster4.setIcon(new ImageIcon(newimg));
			monster4.setBackground(Color.BLACK);
			bigMonstersList.add(monster4);
			monsterPanel.add(monster4);
		} catch (IOException e) {
			LOGGER.error("IOException in line " + Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
		add(monsterPanel);

	}

	private void displayRunnerChooser() {
		JPanel runnerPanel = new JPanel();
		runnerPanel.setBackground(new Color(176, 216, 218));
		runnerPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel label = new JLabel("Choose your runner : ");
		label.setFont(new Font("Serif", Font.BOLD, 40));
		runnerPanel.add(label);
		runner1 = new JButton();
		BufferedImage BI;
		try {
			BI = ImageIO.read(new File("./icon/animation/runner/runner0.png"));
			Image newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40,
					40, Image.SCALE_DEFAULT);
			runner1.setIcon(new ImageIcon(newimg));
			runner1.setBackground(Color.BLACK);
			runnersList.add(runner1);
			runnerPanel.add(runner1);
			runner2 = new JButton();
			BI = ImageIO.read(new File("./icon/animation/runner/runner1.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			runner2.setIcon(new ImageIcon(newimg));
			runner2.setBackground(Color.BLACK);
			runnersList.add(runner2);
			runnerPanel.add(runner2);
			runner3 = new JButton();
			BI = ImageIO.read(new File("./icon/animation/runner/runner2.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			runner3.setIcon(new ImageIcon(newimg));
			runner3.setBackground(Color.BLACK);
			runnersList.add(runner3);
			runnerPanel.add(runner3);
			runner4 = new JButton();
			BI = ImageIO.read(new File("./icon/animation/runner/runner3.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			runner4.setIcon(new ImageIcon(newimg));
			runner4.setBackground(Color.BLACK);
			runnersList.add(runner4);
			runnerPanel.add(runner4);
			runner5 = new JButton();
			BI = ImageIO.read(new File("./icon/animation/runner/runner4.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			runner5.setIcon(new ImageIcon(newimg));
			runner5.setBackground(Color.BLACK);
			runnersList.add(runner5);
			runnerPanel.add(runner5);
			runner6 = new JButton();
			BI = ImageIO.read(new File("./icon/animation/runner/runner5.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			runner6.setIcon(new ImageIcon(newimg));
			runner6.setBackground(Color.BLACK);
			runnersList.add(runner6);
			runnerPanel.add(runner6);
			runner7 = new JButton();
			BI = ImageIO.read(new File("./icon/animation/runner/runner6.png"));
			newimg = BI.getSubimage(0, 0, 31, 32).getScaledInstance(40, 40,
					Image.SCALE_DEFAULT);
			runner7.setIcon(new ImageIcon(newimg));
			runner7.setBackground(Color.BLACK);
			runnersList.add(runner7);
			runnerPanel.add(runner7);
		} catch (IOException e) {
			LOGGER.error("IOException in line " + Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
		add(runnerPanel);

	}

	private void displayWallChooser() {
		JPanel wallPanel = new JPanel();
		wallPanel.setBackground(new Color(176, 216, 218));
		wallPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel label = new JLabel("Choose the shape of your wall : ");
		label.setFont(new Font("Serif", Font.BOLD, 40));
		wallPanel.add(label);
		wall1 = new JButton();
		ImageIcon icon = new ImageIcon("./icon/wall/wall0.png");
		Image img = icon.getImage().getScaledInstance(40, 40,
				Image.SCALE_DEFAULT);
		Image newimg = img.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
		wall1.setIcon(new ImageIcon(newimg));
		wall1.setBackground(Color.BLACK);
		wallsList.add(wall1);
		wallPanel.add(wall1);
		wall2 = new JButton();
		icon = new ImageIcon("./icon/wall/wall1.png");
		img = icon.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT);
		newimg = img.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
		wall2.setIcon(new ImageIcon(newimg));
		wall2.setBackground(Color.BLACK);
		wallsList.add(wall2);
		wallPanel.add(wall2);
		wall3 = new JButton();
		icon = new ImageIcon("./icon/wall/wall2.png");
		img = icon.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT);
		newimg = img.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
		wall3.setIcon(new ImageIcon(newimg));
		wall3.setBackground(Color.BLACK);
		wallsList.add(wall3);
		wallPanel.add(wall3);
		add(wallPanel);
	}

	private void displayDiffcultySpinner() {
		JPanel diffcultyPanel = new JPanel();
		diffcultyPanel.setBackground(new Color(176, 216, 218));
		diffcultyPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		JLabel label = new JLabel("Specify your level of difficulty :   ");
		label.setFont(new Font("Serif", Font.BOLD, 40));
		diffcultyPanel.add(label);
		spinner = new JSpinner(new SpinnerListModel(new String[] { "EASY",
				"MEDIUM", "HARD" }));
		spinner.setEditor(new JSpinner.DefaultEditor(spinner));
		JComponent editor = spinner.getEditor();
		JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor) editor;
		spinnerEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
		spinner.setFont(new Font("Serif", Font.PLAIN, 30));
		spinner.setPreferredSize(new Dimension(160, 80));
		spinner.setValue("EASY");
		Component c = spinner.getEditor().getComponent(0);
		c.setForeground(Color.white);
		c.setBackground(Color.black);
		diffcultyPanel.add(spinner);
		add(diffcultyPanel);
	}

	public int getDifficulty() {
		int difficulty = 2;
		switch (spinner.getValue().toString()) {
		case "HARD":
			difficulty = 6;
			break;
		case "MEDIUM":
			difficulty = 4;
			break;
		case "EASY":
			difficulty = 2;
			break;
		}

		return difficulty;
	}

	public void highlightRunner(JButton runner) {

		Iterator<JButton> itr = runnersList.iterator();
		while (itr.hasNext()) {
			itr.next().setBackground(Color.BLACK);
		}
		runner.setBackground(new Color(222, 222, 222));

	}

	public void highlightBigMonster(JButton monster) {
		Iterator<JButton> itr = bigMonstersList.iterator();
		while (itr.hasNext()) {
			itr.next().setBackground(Color.BLACK);
		}
		monster.setBackground(new Color(222, 222, 222));

	}

	public void highlightsmallMonster(JButton monster1) {
		Iterator<JButton> itr = smallMontersList.iterator();
		while (itr.hasNext()) {
			itr.next().setBackground(Color.BLACK);
		}
		monster1.setBackground(new Color(222, 222, 222));

	}

	public void highlightWall(JButton wall) {
		Iterator<JButton> itr = wallsList.iterator();
		while (itr.hasNext()) {
			itr.next().setBackground(Color.BLACK);
		}
		wall.setBackground(new Color(222, 222, 222));

	}

}
