package mazeGUI;

public class User {
	
	private final String name;
	private final int score;
	
	public User(final String name, final int score) {
		this.name = name;
		this.score = score;
	}
	
	public String getName() {
		return name;
	}
	
	public int getScore() {
		return score;
	}
}
