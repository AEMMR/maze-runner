package sound;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import sun.audio.AudioStream;

@SuppressWarnings("restriction")
public abstract  class Sound {
	static AudioStream audioStream;
	static String filePath;
	
	@SuppressWarnings("static-access")
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public void getSound() {
		InputStream in;
	try {
		in = new FileInputStream(filePath);
		audioStream = new AudioStream(in);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	}
public abstract void doAction();

}
