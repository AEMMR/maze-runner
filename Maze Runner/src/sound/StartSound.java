package sound;

import sun.audio.AudioPlayer;

@SuppressWarnings("restriction")
public class StartSound  extends Sound{
	
	@Override
	public void doAction() {
		getSound();
		AudioPlayer.player.start(audioStream);
		
	}

}
