package view;

import java.awt.Point;
import java.util.ArrayList;

import org.apache.logging.log4j.Logger;

import Model.Bullet;
import Model.EmptyCell;
import Model.Maze;
import Model.Player;
import control.MyLogger;

public class BulletMover extends Thread{
	
	private Maze myMaze;
	private ArrayList<Point> bulletPath;
	private String direction;
	
	private static final Logger LOGGER = MyLogger.getInstance();
	
	public BulletMover(Maze maze, String dir) {
		LOGGER.info("Start moving bullet...");
		this.myMaze = maze;
		this.bulletPath = new ArrayList<>();
		this.direction = dir;
	}
	
	synchronized void shootBullet() {
		Point playerPosition = Player.getInstance().getPoistion();
		int x = playerPosition.x;
		int y = playerPosition.y;
		boolean hit = false;
		Bullet bullet = new Bullet();
		while (!hit) {
			if (direction.equals("sl")) {
				if (y > 0) {
					if (myMaze.getMazeArray()[x][--y].getClass() != EmptyCell.class) {
						if (bullet.collision(myMaze.getMazeArray()[x][y])) {
							moveBullet(bullet);
							myMaze.getMazeArray()[x][y] = new EmptyCell();
						} else {
							moveBullet(bullet);	
						}
						LOGGER.info("And collision.!");
						hit = true;
					} else {
						bulletPath.add(new Point(x, y));
					}
				} else {
					hit = true;
				}
			} else if (direction.equals("sr")) {
				if (y < myMaze.getMazeArray()[0].length - 1) {
					if (myMaze.getMazeArray()[x][++y].getClass() != EmptyCell.class) {
						if (bullet.collision(myMaze.getMazeArray()[x][y])) {
							moveBullet(bullet);
							myMaze.getMazeArray()[x][y] = new EmptyCell();
						} else {
							moveBullet(bullet);
						}
						LOGGER.info("And collision.!");
						hit = true;
					} else {
						bulletPath.add(new Point(x, y));
					}
				} else {
					hit = true;
				}
			} else if (direction.equals("sd")) {
				if (x < myMaze.getMazeArray().length - 1) {
					if (myMaze.getMazeArray()[++x][y].getClass() != EmptyCell.class) {
						if (bullet.collision(myMaze.getMazeArray()[x][y])) {
							moveBullet(bullet);
							myMaze.getMazeArray()[x][y] = new EmptyCell();
						} else {
							moveBullet(bullet);	
						}
						LOGGER.info("And collision.!");
						hit = true;
					} else {
						bulletPath.add(new Point(x, y));
					}
				} else {
					hit = true;
				}
			} else if (direction.equals("su")) {
				if (x > 0) {
					if (myMaze.getMazeArray()[--x][y].getClass() != EmptyCell.class) {
						if (bullet.collision(myMaze.getMazeArray()[x][y])) {
							moveBullet(bullet);
							myMaze.getMazeArray()[x][y] = new EmptyCell();
						} else {
							moveBullet(bullet);	
						}
						hit = true;
						LOGGER.info("And collision.!");
					} else {
						bulletPath.add(new Point(x, y));
					}
				} else {
					hit = true;
				}
			} else {
				break;
			}
		}
		
	}
	
	public void run(){  
		shootBullet();  
	}  
	
	@SuppressWarnings("static-access")
	private void moveBullet(Bullet bullet) {
		for(int i = 0; i < bulletPath.size(); i++) {
			LOGGER.info("Bullet is moving...");
			myMaze.getMazeArray()[bulletPath.get(i).x][bulletPath.get(i).y] = bullet;
			try {
				Thread.currentThread().sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			myMaze.getMazeArray()[bulletPath.get(i).x][bulletPath.get(i).y] = new EmptyCell();
		}
	}
}
