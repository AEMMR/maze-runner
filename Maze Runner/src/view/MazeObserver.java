package view;

import java.awt.event.ActionEvent;

import org.apache.logging.log4j.Logger;

import sound.SoundContext;
import sound.StartSound;
import Model.Maze;
import Model.Player;
import control.Direction;
import control.MyLogger;

public class MazeObserver extends Observer<Maze>{

	private MazeUpdater mazeUpdater;
	private Player mPlayer = Player.getInstance();
	private String currentDirection = "d";
	private BulletMover bm;
	
	private static final Logger LOGGER = MyLogger.getInstance();
	
	public MazeObserver(Maze maze) {
		super(maze);
		this.mazeUpdater = MazeUpdater.getInstance(maze);
		mazeUpdater.start();
	}
	
	@Override
	public void update(ActionEvent actionEvent) {
		final String action = actionEvent.getActionCommand();
		if (action == "VK_DOWN") {
			currentDirection = "d";
			mPlayer.setIcon(mPlayer.grapSprite(Direction.DOWN));
			LOGGER.info("DOWN is pressed.");
			this.observed.moveRunner(currentDirection);
		} else if (action == "VK_UP") {
			currentDirection = "u";
			mPlayer.setIcon(mPlayer.grapSprite(Direction.UP));
			LOGGER.info("UP is pressed.");
			this.observed.moveRunner(currentDirection);
		} else if (action == "VK_RIGHT") {
			currentDirection = "r";
			mPlayer.setIcon(mPlayer.grapSprite(Direction.RIGHT));
			LOGGER.info("RIGHT is pressed.");
			this.observed.moveRunner(currentDirection);
		} else if (action == "VK_LEFT") {
			currentDirection = "l";
			mPlayer.setIcon(mPlayer.grapSprite(Direction.LEFT));
			LOGGER.info("LEFT is pressed.");
			this.observed.moveRunner(currentDirection);
		} else if (action == "VK_SPACE") {
			LOGGER.info("SPACEBAR is pressed.");
			SoundContext context = new SoundContext("./musics/fire.wav");
			StartSound start = new StartSound();
			context.setState(start);	
			context.doAction();
			this.observed.shoot("s" + currentDirection);
		}
	}
	
	public void moveBullet(String dir) {
		this.bm = new BulletMover(observed, dir);
		bm.start();
	}
}
