package view;

import Model.Maze;
import control.Controller;

public class MazeUpdater extends Thread {

	private static Maze myMaze;
	private Controller controller = Controller.controller;
	private static MazeUpdater firstThread = null;

	private MazeUpdater(Maze maze) {
		myMaze = maze;
	}

	public static MazeUpdater getInstance(Maze maze) {
		if (firstThread == null) {
			firstThread = new MazeUpdater(maze);
		} else {
			firstThread.interrupt();
			try {
				firstThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			firstThread = new MazeUpdater(maze);
		}

		return firstThread;
	}

	@SuppressWarnings("static-access")
	synchronized void updateMaze() {
		while (true) {
			myMaze.UpdateMaze();
			controller.updateToolPanel();
			try {
				Thread.currentThread().sleep(400);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	public void run() {
		updateMaze();
	}
}
