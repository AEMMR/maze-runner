package view;

import java.awt.event.ActionEvent;

public abstract class Observer<T> {

	protected T observed;
	
	public Observer(T t) {
		this.observed = t;
	}

	public abstract void update(ActionEvent actionEvent);
}
